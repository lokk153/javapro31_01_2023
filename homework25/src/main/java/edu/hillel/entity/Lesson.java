package edu.hillel.entity;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Lesson {
  long id;
  String name;
  Homework homework;
}
