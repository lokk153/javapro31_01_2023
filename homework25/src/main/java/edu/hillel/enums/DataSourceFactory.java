package edu.hillel.enums;

import com.mysql.cj.jdbc.MysqlDataSource;
import edu.hillel.PropertyReader;
import java.sql.Connection;
import java.util.Properties;
import javax.sql.DataSource;
import lombok.SneakyThrows;


public enum DataSourceFactory {
  INSTANCE;
  private final DataSource dataSource;

  DataSourceFactory() {
    this.dataSource = getDataSource();
  }

  private DataSource getDataSource() {
    MysqlDataSource mysqlDataSource = new MysqlDataSource();
    Properties properties = new PropertyReader().getProperties("application.properties");

    mysqlDataSource.setUrl(properties.getProperty("db.url"));
    mysqlDataSource.setUser(properties.getProperty("db.user"));
    mysqlDataSource.setPassword(properties.getProperty("db.pass"));

    return mysqlDataSource;
  }

  @SneakyThrows
  public Connection getConnection() {
    return dataSource.getConnection();
  }
}
