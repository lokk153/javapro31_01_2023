INSERT INTO Homework (name, description) VALUES
('Math Homework', 'Complete exercises 1-5 on page 42.'),
('English Homework', 'Read Chapter 3 of The Great Gatsby and answer the questions at the end.'),
('Science Homework', 'Write a one-page summary on the process of photosynthesis.');

INSERT INTO Lesson (name, updatedAt, homework_id) VALUES
('Math Lesson 1', '2023-05-11 14:30:00', 1),
('English Lesson 1', '2023-05-12 10:00:00', 2),
('Science Lesson 1', '2023-05-13 09:15:00', 3);