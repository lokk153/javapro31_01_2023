package edu.hillel;

public class Employer {
    private String fullName;
    private String jobTitle;
    private String email;
    private int age;

    public Employer(String fullName, String jobTitle, String email, int age) {
        this.fullName = fullName;
        this.jobTitle = jobTitle;
        this.email = email;
        this.age = age;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employer" + '\n' +
                "Full name = " + fullName + '\n' +
                "Job title = " + jobTitle + '\n' +
                "Email = " + email + '\n' +
                "Age = " + age;
    }
}
