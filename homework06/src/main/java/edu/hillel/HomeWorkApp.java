package edu.hillel;

import java.util.Random;

public class HomeWorkApp {

    public static int getRandomIntInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    //1
    public static void main(String[] args) {
        printThreeWords();
        checkSumSign(getRandomIntInRange(-5, 5), getRandomIntInRange(-5, 5));
        printColor(getRandomIntInRange(-100, 200));
        compareNumbers(getRandomIntInRange(-5, 5), getRandomIntInRange(-5, 5));
        System.out.println(isSumInRange(getRandomIntInRange(0, 10), getRandomIntInRange(0, 10)));
        printNumberType(getRandomIntInRange(-5, 5));
        System.out.println(isNegative(getRandomIntInRange(-5, 5)));
        printString("method printString", getRandomIntInRange(1, 5));
        System.out.println(isLeapYear(getRandomIntInRange(0, 3000)));
    }

    //2
    public static void printThreeWords() {
        System.out.println("Orange\\nBanana\\nApple");
    }

    //3
    public static void checkSumSign(int a, int b) {
        if ((a + b) >= 0) {
            System.out.println("Сума позитивна");
        } else {
            System.out.println("Сума негативна");
        }
    }

    //4
    public static void printColor(int value) {
        if (value <= 0) {
            System.out.println("Червоний");
        } else if (value <= 100) {
            System.out.println("Жовтий");
        } else {
            System.out.println("Зелений");
        }
    }

    //5
    public static void compareNumbers(int a, int b) {
        if (a >= b) {
            System.out.println("a >= b");
        } else {
            System.out.println("a < b");
        }
    }

    //6
    public static boolean isSumInRange(int a, int b) {
        int sum = a + b;
        return (sum) >= 10 && (sum) <= 20;
    }

    //7
    public static void printNumberType(int number) {
        if (number >= 0) {
            System.out.println(number + " - додатнє число");
        } else {
            System.out.println(number + " - від'ємне число");
        }
    }

    //8
    public static boolean isNegative(int number) {
        return number < 0;
    }

    //9
    public static void printString(String string, int quantity) {
        for (int i = 0; i < quantity; i++) {
            System.out.println(string);
        }
    }

    //10
    public static boolean isLeapYear(int year) {
        if (year % 4 != 0) {
            return false;
        } else if (year % 100 != 0) {
            return true;
        } else return year % 400 == 0;
    }
}