package edu.hillel.service;

import edu.hillel.dto.Order;
import edu.hillel.repo.OrderRepository;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class OrderService {
  private final OrderRepository orderRepository = new OrderRepository();

  @WebMethod
  public void add(Order order) {
    orderRepository.addOrder(order);
  }

  @WebMethod
  public Order getById(String id) {
    return orderRepository.getOrderById(id);
  }

  @WebMethod
  public List<Order> getAll() {
    return orderRepository.getAllOrders();
  }
}
