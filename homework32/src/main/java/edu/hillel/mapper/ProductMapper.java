package edu.hillel.mapper;

import edu.hillel.dto.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProductMapper implements RowMapper<Product> {
  @Override
  public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
    return Product.builder()
        .id(rs.getInt("id"))
        .name(rs.getString("name"))
        .price(rs.getDouble("price"))
        .build();
  }
}