package edu.hillel.phonebook;

import lombok.Value;

@Value
public class Record {
  String name;
  String phoneNumber;
}
