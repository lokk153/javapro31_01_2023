package edu.hillel.phonebook;

import java.util.ArrayList;
import java.util.List;
import lombok.Value;

@Value
public class Phonebook {

  List<Record> records;

  public Phonebook() {
    records = new ArrayList<>();
  }

  public void add(Record record) {
    records.add(record);
  }

  public Record find(String name) {
    for (Record record : records) {
      if (record.getName().equals(name)) {
        return record;
      }
    }
    return null;
  }

  public List<Record> findAll(String name) {
    List<Record> recordList = new ArrayList<>();
    for (Record record : records) {
      if (record.getName().equals(name)) {
        recordList.add(record);
      }
    }
    if (!recordList.isEmpty()) {
      return recordList;
    } else {
      return null;
    }
  }
}
