package edu.hillel.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import edu.hillel.entity.Student;

public class HibernateSession {
  private static SessionFactory sessionFactory;

  public static SessionFactory getSessionFactory() {
    if (sessionFactory == null) {
      try {
        sessionFactory = new Configuration().configure()
            .addAnnotatedClass(Student.class)
            .buildSessionFactory();

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return sessionFactory;
  }
}