package edu.hillel.dao;

import edu.hillel.entity.Student;


public class StudentDao extends DefaultDao<Student> {
  public StudentDao() {
    super(Student.class);
  }
}