package edu.hillel;

public class Main {

    public static void main(String[] args) {
        Cat catBoston = new Cat("Boston");
        Cat catKeks = new Cat("Keks");
        Dog dogAlfa = new Dog("Alfa");
        Dog dogBravo = new Dog("Bravo");
        Dog dogCharlie = new Dog("Charlie");

        catBoston.printName();
        catBoston.run(200);
        catBoston.printName();
        catBoston.swim(5);

        catKeks.printName();
        catKeks.run(300);
        catKeks.printName();
        catKeks.swim(10);

        dogAlfa.printName();
        dogAlfa.run(430);
        dogAlfa.printName();
        dogAlfa.swim(10);

        dogBravo.printName();
        dogBravo.run(1000);
        dogBravo.printName();
        dogBravo.swim(11);

        dogCharlie.printName();
        dogCharlie.run(0);
        dogCharlie.printName();
        dogCharlie.swim(0);

        System.out.println("Animal objects have been created = " + Animal.count);
        System.out.println("Cat objects have been created = " + Cat.count);
        System.out.println("Dog objects have been created = " + Dog.count);
    }
}