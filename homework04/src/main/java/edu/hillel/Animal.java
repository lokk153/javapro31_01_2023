package edu.hillel;

public abstract class Animal {
    static int count;

    public Animal(String name) {
        this.name = name;
        count++;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void printName() {
        System.out.print(getName() + " ");
    }

    public abstract void run(int distance);

    public abstract void swim(int distance);
}