package edu.hillel;

public class Dog extends Animal {
    static int count;

    public Dog(String name) {
        super(name);
        count++;
    }

    @Override
    public void run(int distance) {
        int maxDistance = 500;
        if (distance == 0) {
            System.out.println("didn't run");
        } else if (distance <= maxDistance) {
            System.out.println("run " + distance + "m");
        } else {
            System.out.println("can't run more then " + maxDistance + "m");
        }
    }

    @Override
    public void swim(int distance) {
        int maxDistance = 10;
        if (distance == 0) {
            System.out.println("didn't swim");
        } else if (distance <= maxDistance) {
            System.out.println("swim " + distance + "m");
        } else {
            System.out.println("can't swim more then " + maxDistance + "m");
        }
    }
}