package edu.hillel;

public class Cat extends Animal {

    static int count;

    public Cat(String name) {
        super(name);
        count++;
    }

    @Override
    public void run(int distance) {
        int maxDistance = 200;
        if (distance == 0) {
            System.out.println("didn't run");
        } else if (distance <= maxDistance) {
            System.out.println("run " + distance + "m");
        } else {
            System.out.println("can't run more then " + maxDistance + "m");
        }
    }

    @Override
    public void swim(int distance) {
        System.out.println("can't swim");
    }
}