package edu.hillel;

/**
 * homework11.
 */
public class ArrayUtils {

  /**
   * getting a new array from numbers after 4.
   */
  public static int[] getElementsAfterLastFour(int[] array) {
    int lastFourIndex = -1;
    for (int i = array.length - 1; i >= 0; i--) {
      if (array[i] == 4) {
        lastFourIndex = i;
        break;
      }
    }
    if (lastFourIndex == -1) {
      throw new RuntimeException("Array does not contain any fours");
    }
    int countElementsToCopy = array.length - lastFourIndex - 1;

    int[] resultArray = new int[countElementsToCopy];
    System.arraycopy(array, lastFourIndex + 1, resultArray, 0, countElementsToCopy);
    return resultArray;
  }

  /**
   * checks the composition of an array of numbers 1 and 4.
   */
  public static boolean checkArray(int[] array) {
    boolean containsOne = false;
    boolean containsFour = false;

    for (int num : array) {
      if (num == 1) {
        containsOne = true;
      } else if (num == 4) {
        containsFour = true;
      } else {
        return false;
      }
    }
    return containsOne && containsFour;
  }
}