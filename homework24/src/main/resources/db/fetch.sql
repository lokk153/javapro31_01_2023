-- Retrieving all Homework entries
SELECT * FROM Homework;

-- Retrieving all Lesson records, including Homework data
SELECT Lesson.*, Homework.name AS homework_name, Homework.description AS homework_description
FROM Lesson
INNER JOIN Homework ON Lesson.homework_id = Homework.id;

-- Get all Lesson records (including Homework data) sorted by update time
SELECT Lesson.*, Homework.name AS homework_name, Homework.description AS homework_description
FROM Lesson
INNER JOIN Homework ON Lesson.homework_id = Homework.id
ORDER BY Lesson.updatedAt DESC;

-- Retrieving all Schedule entries, including Lesson data
SELECT Schedule.*, Lesson.name AS lesson_name, Lesson.updatedAt AS lesson_updatedAt
FROM Schedule
INNER JOIN Lesson_Schedule ON Schedule.id = Lesson_Schedule.schedule_id
INNER JOIN Lesson ON Lesson_Schedule.lesson_id = Lesson.id;

-- Getting the number of Lessons for each Schedule
SELECT Schedule.*, COUNT(Lesson_Schedule.lesson_id) AS lesson_count
FROM Schedule
LEFT JOIN Lesson_Schedule ON Schedule.id = Lesson_Schedule.schedule_id
GROUP BY Schedule.id;